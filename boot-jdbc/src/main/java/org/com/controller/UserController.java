package org.com.controller;

import org.com.service.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//多数据源及连接池配置	https://www.liangzl.com/get-article-detail-38424.html
//create table user(
//userid int(4) primary key not null auto_increment,
//name varchar(32) not null,
//age varchar(3) not null
//)ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

@RestController
public class UserController {

    @Autowired
    private UserServiceImpl userService;

    @RequestMapping("/createUser")
    public String createUser(String name,Integer age){
//        http://localhost:8080/createUser?name=xiao&age=22
        userService.createUser(name,age);
        return "success";
    }
}
