package org.com.controller;

import org.com.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 异步调用
 *
 *
 */
@RestController
public class MemberController {

    @Autowired
    private MemberService memberService;



    /**
     * http://localhost:8000/addMemberAndEmail
     *
     * 在没有开启 异步调用时打印的顺序是    1 2 3 4
     *
     * @return
     */
    @RequestMapping("/addMemberAndEmail")
    public String addMemberAndEmail(){

        System.out.printf("---->1");

        String ret = memberService.addMemberAndEmail();

        System.out.printf("---->4");

        return ret;
    }

    /**
     * http://localhost:8000/addMemberAsyncEmail
     *
     * 在开启 异步调用时打印的顺序是    ---->1---->4---->2---->3
     *
     * 而且页面也没有结果
     *
     * @return
     */
    @RequestMapping("/addMemberAsyncEmail")
    public String addMemberAsyncEmail(){
        System.out.printf("---->1");

        String ret = memberService.addMemberAsyncEmail();

        System.out.printf("---->4");

        return ret;
    }
}
