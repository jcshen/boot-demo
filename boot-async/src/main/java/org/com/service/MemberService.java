package org.com.service;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class MemberService {

    public String addMemberAndEmail(){
        System.out.printf("---->2");

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.printf("---->3");

        return "同步调用完成";
    }

    /**
     *  @Async  注解 相当于这个方法是单独线程在执行
     *
     *
     * @return
     */
    @Async
    public String addMemberAsyncEmail(){
        System.out.printf("---->2");

        try {
            Thread.sleep(5000);
            //TODO
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.printf("---->3");

        return "异步调用完成";
    }
}
