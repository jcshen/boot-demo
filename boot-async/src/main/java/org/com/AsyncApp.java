package org.com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * 使用 @Async 实现异步调用
 *
 *
 */
@SpringBootApplication
@EnableAsync//开启异步调用
public class AsyncApp {

    public static void main(String[] args) {
        SpringApplication.run(AsyncApp.class);
    }
}
