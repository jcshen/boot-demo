package org.com.model;

import lombok.Data;

/**
 * CREATE DATABASE test;
 *
 * CREATE TABLE test.t_user(
 *   userId INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 *   userName VARCHAR(255) NOT NULL ,
 *   password VARCHAR(255) NOT NULL ,
 *   phone VARCHAR(255) NOT NULL
 * ) ENGINE=INNODB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8;
 */
@Data
public class UserInfo {

    private Integer userId;

    private String userName;

    private String password;

    private String phone;
}
