package org.com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataSourcesApp {

    public static void main(String[] args) {
        SpringApplication.run(DataSourcesApp.class, args);
    }
}
