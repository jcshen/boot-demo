package org.com.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.com.model.UserInfo;

import java.util.List;

@Mapper
public interface UserInfoDao {

    int insert(UserInfo record);

    void deleteUserById(@Param("userId") Integer userId);

    void updateUser(UserInfo userDomain);

    List<UserInfo> selectUsers();
}
