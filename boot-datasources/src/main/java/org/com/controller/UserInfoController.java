package org.com.controller;

import org.com.model.UserInfo;
import org.com.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @RequestParam 用于将请求参数区数据映射到功能处理方法的参数上，value：参数名字，即入参的请求参数名字，
 * 如userName表示请求的参数区中的名字为userName的参数的值将传入，required：是否必须，默认是true，
 * 表示请求中一定要有相应的参数，否则将报404错误码；
 */

@RestController
//@RequestMapping("/user")
public class UserInfoController {

//    TODO：准备开发个多数据源

    @Autowired
    private UserInfoService userService;

    //    @PostMapping(value = "/addUser")
    @RequestMapping("/addUser")//http://localhost:8080/addUser?userName=xiao&password=123456&phone=13418921473
    public ResponseEntity addUser(
            @RequestParam(value = "userName", required = true)
                    String userName,
            @RequestParam(value = "password", required = true)
                    String password,
            @RequestParam(value = "phone", required = false)
                    String phone
    ) {
        UserInfo userDomain = new UserInfo();
        userDomain.setUserName(userName);
        userDomain.setPassword(password);
        userDomain.setPhone(phone);
        userService.insert(userDomain);
        return ResponseEntity.ok("添加成功");
    }

    @DeleteMapping("/deleteUser")
    public ResponseEntity deleteUser(@RequestParam(value = "userId", required = true) Integer userId) {

        userService.deleteUserById(userId);
        return ResponseEntity.ok("删除成功");
    }

    @PutMapping("/updateUser")
    public ResponseEntity updateUser(
            @RequestParam(value = "userId", required = true)
                    Integer userId,
            @RequestParam(value = "userName", required = false)
                    String userName,
            @RequestParam(value = "password", required = false)
                    String password,
            @RequestParam(value = "phone", required = false)
                    String phone
    ) {
        UserInfo userDomain = new UserInfo();
        userDomain.setUserId(userId);
        userDomain.setUserName(userName);
        userDomain.setPassword(password);
        userDomain.setPhone(phone);
        userService.updateUser(userDomain);
        return ResponseEntity.ok("更新成功");
    }

    @GetMapping("/getUsers")
    public ResponseEntity getUsers() {
        return ResponseEntity.ok(userService.selectUsers());
    }
}
