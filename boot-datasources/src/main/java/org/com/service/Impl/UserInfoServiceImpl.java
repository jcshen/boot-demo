package org.com.service.Impl;

import org.com.dao.UserInfoDao;
import org.com.model.UserInfo;
import org.com.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserInfoServiceImpl implements UserInfoService {

    @Autowired
    private UserInfoDao userDao;
    @Override
    public int insert(UserInfo record) {
        return userDao.insert(record);
    }

    @Override
    public void deleteUserById(Integer userId) {
        userDao.deleteUserById(userId);
    }

    @Override
    public void updateUser(UserInfo userDomain) {
        userDao.updateUser(userDomain);
    }

    @Override
    public List<UserInfo> selectUsers() {
        return userDao.selectUsers();
    }
}
