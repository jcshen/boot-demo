package org.com.service;

import org.com.model.UserInfo;

import java.util.List;

public interface UserInfoService {
    int insert(UserInfo record);

    void deleteUserById(Integer userId);

    void updateUser(UserInfo userDomain);

    List<UserInfo> selectUsers();

}
