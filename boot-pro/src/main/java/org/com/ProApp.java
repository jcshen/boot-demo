package org.com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 多环境配置
 *  application-dev.properties  :开发环境
 *  application-test.properties :测试环境
 *  application-prod.properties :生产环境
 *
 *  更改 application.properties 中的属性
 *  spring.profiles.ative=dev 来决定读取那个环境中的配置
 *
 *
 */
@SpringBootApplication
public class ProApp {
    public static void main(String[] args) {
        SpringApplication.run(ProApp.class);
    }
}
