package org.com.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PropertiesController {

    @Value("${http_url}")
    private String httpUrl;

    /**
     * http://localhost:8082/getUrl
     * http://localhost:8083/getUrl
     *
     * @return
     */
    @RequestMapping("/getUrl")
    public String getUrl(){
        return httpUrl;
    }
}
