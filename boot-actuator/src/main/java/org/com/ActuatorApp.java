package org.com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 监控中心
 *  Actuator没有页面,返回json
 * 访问监控中心
 *
 * http://localhost:8080/actuator/beans
 * http://localhost:8080/actuator/health
 * http://localhost:8080/actuator/env
 *
 *
 */
@SpringBootApplication
public class ActuatorApp {

    public static void main(String[] args) {
        SpringApplication.run(ActuatorApp.class,args);
    }
}
