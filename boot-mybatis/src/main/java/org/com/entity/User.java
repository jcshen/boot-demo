package org.com.entity;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;


/**
 * create table user(
 * id int (4) primary key not null auto_increment,
 * name varchar(32),
 * age int(3)
 * );
 */

//@Getter
//@Setter
@Data
public class User {

    private Integer id;
    private Integer age;
    private String name;
}
