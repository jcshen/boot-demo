package org.com.service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.com.entity.User;
import org.com.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Slf4j
public class UserService {

    @Autowired
    private UserMapper userMapper;

    @Transactional//开启事务
    public int insertUser(String name, Integer age) {
        int insert = userMapper.insert(name, age);
//        int a = 1 / 0;
        log.info("##########insert:{}##########", insert);
        return insert;

    }


    /**
     * 分页查询
     *
     * @param page     当前页
     * @param pageSize 分页条数
     * @return
     */
    public PageInfo<User> findUserList(int page, int pageSize) {

        PageHelper.startPage(page, pageSize);//底层实现原理采用改写SQL语句

        List<User> userList = userMapper.findUserList();
        //返回给客户端展示
        PageInfo<User> userPageInfo = new PageInfo<>(userList);
        return userPageInfo;

    }
}
