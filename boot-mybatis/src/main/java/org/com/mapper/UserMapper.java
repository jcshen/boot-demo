package org.com.mapper;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.com.entity.User;

import java.util.List;

//@Mapper
public interface UserMapper {

    @Select("select id,name,age from user")
    List<User> findUserList();

    @Select("SELECT * FROM USER WHERE NAME = #{name}")
    User findByName(@Param("name") String name);

    @Insert("Insert into user(NAME,AGE) Values(#{name},#{age})")
    int insert(@Param("name") String name, @Param("age") Integer age);
}
