package org.com;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 1、mybatis启动方式可以在 mapper 层不需要加 @Mapper 注解。但是一定要在启动类加上 @MapperScan
 *
 * 2、mybatis在mybatis接口上加 @Mapper 注入mybatis容器，就不需要在启动类上加 @MapperScan
 *
 * 3、多数据源：垂直 根据业务划分具体数据库；水平
 *    一个项目中有多个数据源：无限大；具体多少根据内存大小
 *
 *   多数据源如何划分：分包名（业务）||注解方式
 *   org.com.test1---->datasource1|
 *                                |类似 多个不同jar包，不同业务需求。多个不同业务需求，存放同一个项目中
 *   org.com.test2---->datasource2|
 *
 *
 *
 *
 */
@SpringBootApplication
@MapperScan(basePackages = {"org.com.mapper"})//interface 上不用加注解，这里直接配置要扫描的包。通过反射技术来实现的
public class MybatisApp {

    public static void main(String[] args) {
        SpringApplication.run(MybatisApp.class,args);

    }
}
