package org.com.controller;

import com.github.pagehelper.PageInfo;
import org.com.entity.User;
import org.com.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    // http://localhost:8080/insertUser?name=springboot&age=2
    @RequestMapping("/insertUser")
    public Integer insertUser(String name, Integer age) {
        return userService.insertUser(name, age);
    }


    //http://localhost:8080/findUserList?page=1&pageSize=2
    @RequestMapping("/findUserList")
    public PageInfo<User> findUserList(int page, int pageSize) {
        return userService.findUserList(page, pageSize);
    }

}
