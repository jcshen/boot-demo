package org.com;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableAutoConfiguration
@EnableAdminServer
public class AdminUiServer {

    /**
     * 先启动server端
     * 访问
     *  http://localhost:8080/applications
     * @param args
     */
    public static void main(String[] args) {
        SpringApplication.run(AdminUiServer.class, args);
    }
}
