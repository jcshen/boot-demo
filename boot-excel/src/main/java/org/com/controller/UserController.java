package org.com.controller;

import org.com.model.User;
import org.com.service.UserService;
import org.com.service.impl.ExcelServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * @Auther: jcshen
 * @Date: 2020/3/23 22:41
 * @Description:
 */
@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private ExcelServiceImpl excelService;

    @RequestMapping("/index")
    public String index() {
        return "index";
    }

    @RequestMapping("/import")
    @ResponseBody
    public String excelImport(@RequestParam(value = "filename") MultipartFile file) throws Exception {

//		String fileName = file.getOriginalFilename();

        int result = 0;

        List<User> list = excelService.readExcelFileToDTO(file, User.class);

        System.out.println("长度：" + list.size());

//        List<User> userList = list.stream().map(dto -> {
//            User user = userService.selectUser(dto.getId());
//            if(user == null) {
//                user = new User();
//            }
//            user.setId(dto.getId());
//            user.setName(dto.getName());
//            user.setAge(dto.getAge());
//            return user;
//        }).collect(Collectors.toList());
//        userService.saveAll(userList);
        return "";


    }

}
