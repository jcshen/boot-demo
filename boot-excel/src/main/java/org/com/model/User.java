package org.com.model;

import lombok.Data;
import org.com.dao.Description;

@Data
public class User {
//    private String uid;
//    private String uname;
//    private String passwd;
//    private int age;
//    private String address;
//    private String job;

    @Description(showName = "编号")
    private String id;

    @Description(showName = "用户名称")
    private String name;

    @Description(showName = "用户年龄")
    private String age;
}
