package org.com.dao;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

/**
 * @Auther: jcshen
 * @Date: 2020/3/23 22:24
 * @Description: 添加元注解
 */
@Target({TYPE, ElementType.FIELD})
@Retention(RUNTIME)
public @interface Description {
    String showName() default "";
}
