package org.com.dao;

import org.com.model.User;

public interface UserMapper {

    int addUser(User user);
    User selectUser(String uid);
    int updateUser(User user);
}
