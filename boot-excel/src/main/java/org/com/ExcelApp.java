package org.com;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan(" org.com.dao")
public class ExcelApp {
    public static void main(String[] args) {
        SpringApplication.run(ExcelApp.class,args);
    }
}
