package org.com.service;

import org.com.model.User;

public interface UserService {

    int addUser(User user);

    User selectUser(String uid);

    int updateUser(User user);
}
