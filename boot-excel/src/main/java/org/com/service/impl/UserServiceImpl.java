package org.com.service.impl;

import org.com.dao.UserMapper;
import org.com.model.User;
import org.com.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;


    @Override
    public int addUser(User user) {
        userMapper.addUser(user);
        return 0;
    }

    @Override
    public User selectUser(String uid) {
        return null;
    }

    @Override
    public int updateUser(User user) {
        return 0;
    }
}
