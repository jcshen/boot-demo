package org.com.utils;


import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExcelUtils {

    public static final String XLS = ".xls";
    public static final String XLSX = ".xlsx";


    private List<Map<String, String>> sheetToMap(Sheet sheet) {
        List<Map<String, String>> list = new ArrayList<Map<String, String>>();
        int rows = sheet.getPhysicalNumberOfRows();
        String[] titles = getSheetRowValues(sheet.getRow(0));//第一行作为表头

        for (int i = 1; i < rows; i++) {
            String[] values = getSheetRowValues(sheet.getRow(i));
            Map<String, String> map = new HashMap<String, String>();
            for (int j = 0; j < titles.length; j++) {
                map.put(titles[j], values[j]);
            }
            list.add(map);
        }
        return list;
    }


    private String[] getSheetRowValues(Row row) {
        if (row == null) {
            return new String[]{};
        } else {
            int cellNum = row.getLastCellNum();
            List<String> cellList = new ArrayList<String>();
            for (int i = 0; i < cellNum; i++) {
                cellList.add(getValueOnCell(row.getCell(i)));
            }
            return cellList.toArray(new String[0]);
        }
    }


    private String getValueOnCell(Cell cell) {
        if (cell == null) {
            return "";
        }
        switch (cell.getCellType()) {
            case STRING:
                return cell.getStringCellValue();
            case NUMERIC:
                return String.format("%.2f", cell.getNumericCellValue());
            case BOOLEAN:
                return cell.getBooleanCellValue() ? "true" : "false";
            case FORMULA:
                try {
                    return cell.getStringCellValue();
                } catch (Exception e) {
                    return String.format("%.2f", cell.getNumericCellValue());
                }
            default:
                return "";
        }
    }

    /**
     * 类型转换
     *
     * @param file
     * @return
     * @throws Exception
     */
    private Workbook buildWorkbook(MultipartFile file) throws Exception {
        String filename = file.getOriginalFilename();
        if (filename.endsWith(XLS)) {
            return new HSSFWorkbook(file.getInputStream());
        } else if (filename.endsWith(XLS)) {
            return new XSSFWorkbook(file.getInputStream());
        } else {
            throw new Exception("unknow file format: " + filename);
        }
    }


}
