package org.com.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

@RestController//修饰的类下的方法，全部返回json格式，这样就不用在方法上加@ResponseBody
public class IndexController {

    @RequestMapping("/index")
    public String index() {
        String time = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
        return time;
    }

    @RequestMapping("/getMember")
    public Map<String, Object> getMember() {
        Map<String, Object> map = new HashMap<>();
        String time = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
        map.put("error", "404");
        map.put("时间", time);
        return map;


    }
}
