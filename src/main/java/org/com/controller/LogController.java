package org.com.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 测试 AOP 打印日志
 *
 * http://localhost:8080/getLog?name=jc&age=20
 *
 * 查看控制台的日志打印情况
 */
@RestController
public class LogController {


    @RequestMapping("/getLog")
    public String getLog(String name,Integer age){
        return "success";
        //http://localhost:8080/getLog?name=jc&age=20
    }
}
