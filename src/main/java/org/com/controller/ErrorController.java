package org.com.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ErrorController {

    @RequestMapping("/getUser")
    public String getUser(int i){
        //这里不能 try catch；否则就全局捕获不到异常
        int j= 1/i;
        return "success---->"+i;

//        http://localhost:8080/getUser?i=0
    }
}
