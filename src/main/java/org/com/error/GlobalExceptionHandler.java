package org.com.error;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

/**
 * AOP 技术
 * 全局捕获异常
 *
 * 两种处理方式：
 *1：捕获异常返回json格式
 *2：捕获异常返回页面
 *
 */

@ControllerAdvice(basePackages = "org.com.controller")
public class GlobalExceptionHandler {

    //modelAndView  //返回页面
    @ExceptionHandler(RuntimeException.class)   //拦截的异常是 运行时异常
    @ResponseBody   //返回json
    public Map<String,Object> errorResult(){
        //实际项目中，将错误记录在日志中
        Map<String,Object> errorMap= new HashMap<String ,Object>();
        errorMap.put("errorCode","500");
        errorMap.put("errorMsg","全局捕获系统错误");
        return errorMap;
    }

}
