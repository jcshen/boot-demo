package org.com.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 创建Springboot整合 jsp；一定要为 war类型，否则找不到页面
 * 不要把 JSP 放到resource下，会访问不到的
 *
 * https://www.cnblogs.com/xingyunblog/p/8727079.html
 */
@Controller//不能用ResController
public class JspController {

    @RequestMapping("/jspIndex")
    public String jspIndex() {
        return "jspIndex";
        //http://localhost:8000/jspIndex
    }
}
