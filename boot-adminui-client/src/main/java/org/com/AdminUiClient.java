package org.com;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动 client 后查看 admin ui 页面
 *
 * 没成功
 */
@SpringBootApplication
public class AdminUiClient {

    public static void main(String[] args) {
        SpringApplication.run(AdminUiClient.class, args);
    }
}
