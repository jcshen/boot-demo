package org.com.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {

    @RequestMapping("/indexDev")
    public String indexDev(){
        String result ="springboot v 2.2.4";
        return result;
    }
}
