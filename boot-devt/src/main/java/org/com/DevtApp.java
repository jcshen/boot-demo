package org.com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 热部署
 * Devtools原理
 * 1：devtools监听classpath下的文件变动，并会立即重启应用
 */

@SpringBootApplication
public class DevtApp {

    public static void main(String[] args) {
        SpringApplication.run(DevtApp.class,args);
    }
}
